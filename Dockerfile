FROM nginx:1.20.1-alpine

RUN rm -rf /usr/share/nginx/html/*

COPY $PWD/site-content /usr/share/nginx/html
